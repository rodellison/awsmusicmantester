package MusicManTester;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.util.Properties;


public class MusicManGuiTester extends Application {

    //Main Tester Window
    public  static String strSongKickAPIID = "";

    private Stage myMainStage;
    private String strFileToUpload = "";

    @Override
    public void start(Stage primaryStage) throws Exception{

        Properties prop = new Properties();
        try {
            try {
                // load a properties file
                prop.load(getClass().getResourceAsStream("config.properties"));
                if (prop.size() > 0) {
                    strSongKickAPIID = prop.getProperty("apikey");
                }
                else {
                    System.out.println("Can't find config.properties file.");
                }
             } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
         } catch (Exception ex) {

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Exception");
                alert.setHeaderText("MusicManTester Exception");
                alert.setContentText(ex.getMessage());
                alert.showAndWait();
        }

        Parent root = FXMLLoader.load(getClass().getResource("MusicManTesterForm.fxml"));
        myMainStage = primaryStage;
        myMainStage.setTitle("MusicManTester");

        myMainStage.setScene(new Scene(root));
        myMainStage.show();

    }

    public static void main(String[] args) {
        launch(args);

    }


}
