package MusicManTester;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


//GUI imports
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.*;
import java.net.URLEncoder;


public class MusicManGuiTestController implements Initializable {

    //Main Tester Window
    public String strSongKickAPIID = "";
    public  Label lblStatus;
    public  Label lblAPIID;
    public  TextArea txtArea;
    public  TextField txtSongKickAPIURL;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        lblAPIID.setText("APIID="+MusicManGuiTester.strSongKickAPIID);
        txtArea.appendText("Hello");
        txtArea.setWrapText(true);

    }

    public void menuHandler (ActionEvent myActionEvent) {
        String strMenuItem = ((MenuItem) myActionEvent.getSource()).getText();

        switch (strMenuItem) {
            case "Exit":
                ExitApp();
                break;
            case "About":
                askAbout();
                break;

        }

    }
    public void buttonHandler (ActionEvent myActionEvent) {
        String strMenuItem = ((Button) myActionEvent.getSource()).getText();
        String strURLEncodedParm = "";

        try {
            strURLEncodedParm = URLEncoder.encode(txtSongKickAPIURL.getText(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            UpdateTextArea("Error in URLEncoding the parm: " + txtSongKickAPIURL.getText(), true);
            return;
        }

        switch (strMenuItem) {

            case "Fetch Venue":
                doFetch("http://api.songkick.com/api/3.0/search/venues.json?query=" + strURLEncodedParm + "&apikey=" + MusicManGuiTester.strSongKickAPIID);
                break;
            case "Fetch Artist":
                doFetch("http://api.songkick.com/api/3.0/search/artists.json?query=" + strURLEncodedParm + "&apikey=" + MusicManGuiTester.strSongKickAPIID);
                break;

        }

    }

  public void UpdateTextArea(String myUpdate, boolean doAppend) {


      Platform.runLater(new Runnable() {
          @Override
          public void run() {

              java.util.Date date= new java.util.Date();
              Timestamp tempTimeStamp = new Timestamp(date.getTime());
              if (doAppend) {
                  txtArea.appendText("\n" + tempTimeStamp + "\n" + myUpdate);
                  return;
              }
              txtArea.setText("\n" + tempTimeStamp + "\n" + myUpdate);
              return;
          }
      });
  }

    public void doFetch  (String thisURI) {

         //Create a task thread to run this longer running bit of code on..
        //Also used to allow txtArea updates on the main form.
        Task<Boolean> task = new Task<Boolean>() {
            public Boolean call() throws Exception {

                String strVenueID = "";
                String strArtistID = "";

                CloseableHttpClient httpclient = HttpClients.createDefault();
                try {

                    String strRequestString = thisURI;
                    if (strRequestString.isEmpty()) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Exception");
                        alert.setHeaderText("MusicManTester Exception");
                        alert.setContentText("Need to provide a URL to send..");
                        alert.showAndWait();
                        return null;
                    }

                    HttpGet httpget = new HttpGet(strRequestString);
                    UpdateTextArea("Executing request: " + httpget.getRequestLine() + "\n", false);

                    // Create a custom response handler
                    ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                        @Override
                        public String handleResponse(
                                final HttpResponse response) throws ClientProtocolException, IOException {
                            int status = response.getStatusLine().getStatusCode();
                            if (status >= 200 && status < 300) {
                                HttpEntity entity = response.getEntity();
                                return entity != null ? EntityUtils.toString(entity) : null;
                            } else {
                                throw new ClientProtocolException("Unexpected response status: " + status);
                            }
                        }

                    };

                    String responseBody = httpclient.execute(httpget, responseHandler);
                    UpdateTextArea("--------------------------------------------------------\n", true);
                    UpdateTextArea(responseBody + "\n", true);

                    //Jackson JSON
                    ObjectMapper mapper=new ObjectMapper();
                    JsonFactory factory=mapper.getFactory();
                    JsonParser jp=factory.createParser(responseBody);
                    jp.skipChildren();
                    JsonNode input=mapper.readTree(jp);

                    if (strRequestString.contains("venues.json")) {
                        //Parse the response as a Venue call, get the Venue's ID
                        try {

                            JsonNode myVenueNode =  input.get("resultsPage").get("results").get("venue").get(0);
                            strVenueID = myVenueNode.get("id").asText();
                            UpdateTextArea("The venue id is: " + strVenueID + "\n", true);

                        } catch (Exception ex) {
                            UpdateTextArea("Exception occurred in jsonpath " + ex.getMessage(), true);
                            return null;

                        }
                        UpdateTextArea("--------------------------------------------------------\n", true);
                        //Now us the venue ID to get the calendar for the venue
                        String strVenueCalendrRequest = "http://api.songkick.com/api/3.0/venues/" + strVenueID + "/calendar.json?apikey=" + MusicManGuiTester.strSongKickAPIID;
                        httpget = new HttpGet(strVenueCalendrRequest);
                        UpdateTextArea("Executing request for Venue Calendar :" + httpget.getRequestLine() + "\n", true);
                        responseBody = httpclient.execute(httpget, responseHandler);

                        UpdateTextArea("--------------------------------------------------------\n", true);
                        UpdateTextArea(responseBody + "\n", true);
                        UpdateTextArea("--------------------------------------------------------\n", true);

                        jp=factory.createParser(responseBody);
                        input=mapper.readTree(jp);
                        UpdateTextArea("Upcoming events:\n", true);


                        try {
                            int intTotalEntries = input.get("resultsPage").findValue("totalEntries").asInt();
                            if (intTotalEntries > 0) {

                                JsonNode strVenueCalendarValues1 = input.get("resultsPage").get("results").findValue("event");
                                for(Iterator<JsonNode> i = strVenueCalendarValues1.iterator(); i.hasNext(); ) {
                                    JsonNode item = i.next();
                                    UpdateTextArea(item.get("displayName").asText() + "\n", true);
                                }

                            } else {
                                UpdateTextArea("I don't see any upcoming events\n", true);
                            }
                         } catch (Exception ex) {
                            UpdateTextArea("Error processing the json parse for venue calendar. " + ex.getMessage() + "\n", true);
                         }
                        return  null;
                    } // end If Venue
                    else {
                        //Parse the response as a Artist call, get the Artist's ID
                        try {
                            JsonNode myArtistNode =  input.get("resultsPage").get("results").get("artist").get(0);
                            strArtistID = myArtistNode.get("id").asText();
                            UpdateTextArea("The Artist id is: " + strArtistID + "\n", true);
                        } catch (Exception ex) {
                            UpdateTextArea("Exception occurred in Artist jsonpath " + ex.getMessage() + "\n", true);
                            return null;

                        }
                        UpdateTextArea("--------------------------------------------------------\n", true);

                        //Now use the Artist ID to get their calendar of upcoming events
                        String strVenueCalendrRequest = "http://api.songkick.com/api/3.0/artists/" + strArtistID + "/calendar.json?apikey=" + MusicManGuiTester.strSongKickAPIID;
                        httpget = new HttpGet(strVenueCalendrRequest);
                        UpdateTextArea("Executing request for Artist Calendar :" + httpget.getRequestLine() + "\n", true);
                        responseBody = httpclient.execute(httpget, responseHandler);

                        UpdateTextArea("--------------------------------------------------------\n", true);
                        UpdateTextArea(responseBody + "\n", true);
                        UpdateTextArea("--------------------------------------------------------\n", true);

                        jp=factory.createParser(responseBody);
                        input=mapper.readTree(jp);

                        UpdateTextArea("Upcoming events:\n", true);

                        try {
                            int intTotalEntries = input.get("resultsPage").findValue("totalEntries").asInt();
                            if (intTotalEntries > 0) {
                                JsonNode strArtistCalendarValues1 = input.get("resultsPage").get("results").findValue("event");
                                for(Iterator<JsonNode> i = strArtistCalendarValues1.iterator(); i.hasNext(); ) {
                                    JsonNode item = i.next();
                                    JsonNode cityItem = item.get("location");
                                    UpdateTextArea(item.get("displayName").asText() + " in " + cityItem.get("city").asText() +"\n", true);
                                }

                            } else {
                                UpdateTextArea("I don't see any upcoming events\n", true);
                            }
                        } catch (Exception ex) {
                            UpdateTextArea("Error processing the json parse for artist calendar. " + ex.getMessage() + "\n", true);
                        }
                        return  null;

                    } //end If Artist
                }catch(JsonParseException e){
                    e.printStackTrace();
                }
                catch(JsonMappingException e){
                    e.printStackTrace();

                } catch (IOException ex) {
                    UpdateTextArea("--------------------------------------------------------\n", true);
                    UpdateTextArea(ex.getMessage(), true);

                } finally {
                    try {
                        httpclient.close();

                    } catch (IOException ex) {
                        UpdateTextArea("--------------------------------------------------------\n", true);
                        UpdateTextArea(ex.getMessage(), true);
                    }
                }
            return null;
            }
        };
        new Thread(task).start();
    }

    public void askAbout  () {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About Dialog");
        alert.setHeaderText("MusicMan, a skill for Alexa");
        alert.setContentText("created by Rod Ellison 5/20/2016");
        alert.showAndWait();

    }

    public void ExitApp() {

        Platform.exit();
    }
}
