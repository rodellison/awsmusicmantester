# SongKick - Alexa Music Man Tester

##MusicMan Test app Documentation
This project is a JavaFX Gui Test app, to allow for seperate testing of the SongKick API calls that are used in the Amazon Echo/Alexa "The Music Man" skill.

See http://www.songkick.com/
http://www.songkick.com/developer

![Image of Songkick ](http://static.tumblr.com/yfms2o4/HKnl73h3y/logo_for_tumblr_fullname.png) 

## Contents
The project includes the following:
 - **MusicManTester** - a JAVAFA test app to allow simple tests of an Artist or a Venue, executed via an appropirate Fetch Artist, or Fetch Venue button with full json results
 displayed in a central text area gui.


### Required for the projects to run
A config.properties is needed to house the Songkick API credential - so as to not
 be hard coded inside the app.  This file should be placed inside the musicman dir
 as well as should be in the same dir as the output classes (jarred)

config.properties should have the following entry:

apikey="your Songkick API id"